package com.realdolmen.numbers;

import com.realdolmen.extensions.CustomExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;

@ExtendWith(CustomExtension.class)
public class FractionTest {
    // ######### 1. Basic JUnit #########
    @Test
    public void testGetNumeratorThatItReturnsTheNumerator4Simplified() {
        Fraction fraction = new Fraction(4, 12);
        Assertions.assertEquals(1, fraction.getNumerator());
    }

    @Test
    public void testGetDenominatorThatItReturnsTheDenominator12Simplified() {
        Fraction fraction = new Fraction(4, 12);
        Assertions.assertEquals(3, fraction.getDenominator());
    }

    @Test
    public void testGetNumeratorThatItReturnsAFractionWithoutSimplification() {
        Fraction fraction = new Fraction(1, 2);
        Assertions.assertAll(
                () -> Assertions.assertEquals(1, fraction.getNumerator()),
                () -> Assertions.assertEquals(2, fraction.getDenominator())
        );
    }


    @Test
    public void testToStringThatItReturnsASimplifiedFraction() {
        Fraction fraction = new Fraction(4, 12);
        Assertions.assertEquals("1/3", fraction.toString());
    }

    @Test
    public void testToStringThatItReturnsASimplifiedFractionWithDoubles() {
        Fraction fraction = new Fraction(4.5, 12.5);
        Assertions.assertEquals("9/25", fraction.toString());
    }

    @Test
    public void testToStringThatItReturnsASimplifiedFractionWithStrings() {
        Fraction fraction = new Fraction("4.5", "12.5");
        Assertions.assertEquals("9/25", fraction.toString());
    }

    @Test
    public void testToStringThatItThrowsAnExceptionWithAWrongInputParameter() {
        NumberFormatException exception = Assertions.assertThrows(NumberFormatException.class, () -> new Fraction("Wrong", "12.5"));
        Assertions.assertEquals("Cannot convert String to a number", exception.getMessage());
    }

    @Test
    public void testThatAFractionObjectWithDenominator0ReturnsAnException() {
        ArithmeticException exception = Assertions.assertThrows(ArithmeticException.class, () -> new Fraction(8, 0), "denominator is 0");
        Assertions.assertEquals("Cannot divide by 0", exception.getMessage());
    }

    @Test
    public void testThatAFractionObjectWithDenominatorAsString0ReturnsAnException() {
        ArithmeticException exception = Assertions.assertThrows(ArithmeticException.class, () -> new Fraction("8", "0"), "denominator is 0");
        Assertions.assertEquals("Cannot divide by 0", exception.getMessage());
    }

    @Test
    public void testThatAFractionObjectWithDenominatorDouble0ReturnsAnException() {
        ArithmeticException exception = Assertions.assertThrows(ArithmeticException.class, () -> new Fraction(8.0, 0.0), "denominator is 0");
        Assertions.assertEquals("Cannot divide by 0", exception.getMessage());
    }

    // ######### 2. Advanced JUnit #########
    @ParameterizedTest
    @MethodSource("com.realdolmen.numbers.ParametersForFractionExtension#summation")
    public void testAddReturnsASummationOf2Fractions(double nominator1, double denominator1, double nominator2, double denominator2, String result) {
        Fraction a = new Fraction(nominator1, denominator1);
        Fraction b = new Fraction(nominator2, denominator2);
        Fraction c = a.add(b);
        Assertions.assertAll(
                () -> Assertions.assertNotSame(c, a),
                () -> Assertions.assertNotSame(c, b),
                () -> Assertions.assertEquals(result, c.toString())
        );
    }

    @ParameterizedTest
    @MethodSource("com.realdolmen.numbers.ParametersForFractionExtension#subtraction")
    public void testSubtractReturnsASubtractionOf2Fractions(double nominator1, double denominator1, double nominator2, double denominator2, String result) {
        Fraction a = new Fraction(nominator1, denominator1);
        Fraction b = new Fraction(nominator2, denominator2);
        Fraction c = a.subtract(b);
        Assertions.assertAll(
                () -> Assertions.assertNotSame(c, a),
                () -> Assertions.assertNotSame(c, b),
                () -> Assertions.assertEquals(result, c.toString())
        );
    }

    @ParameterizedTest
    @MethodSource("com.realdolmen.numbers.ParametersForFractionExtension#multiplication")
    public void testMultiplyReturnsAMultiplicationOf2Fractions(double nominator1, double denominator1, double nominator2, double denominator2, String result) {
        Fraction a = new Fraction(nominator1, denominator1);
        Fraction b = new Fraction(nominator2, denominator2);
        Fraction c = a.multiply(b);
        Assertions.assertAll(
                () -> Assertions.assertNotSame(c, a),
                () -> Assertions.assertNotSame(c, b),
                () -> Assertions.assertEquals(result, c.toString())
        );
    }

    @ParameterizedTest
    @MethodSource("com.realdolmen.numbers.ParametersForFractionExtension#division")
    public void testDivideReturnsADivisionOf2Fractions(double nominator1, double denominator1, double nominator2, double denominator2, String result) {
        Fraction a = new Fraction(nominator1, denominator1);
        Fraction b = new Fraction(nominator2, denominator2);
        Fraction c = a.divide(b);
        Assertions.assertAll(
                () -> Assertions.assertNotSame(c, a),
                () -> Assertions.assertNotSame(c, b),
                () -> Assertions.assertEquals(result, c.toString())
        );
    }

    @Test
    public void testToDouble() {
        Fraction fraction = new Fraction(7, 2);
        Assertions.assertEquals(3.5, fraction.toDouble());
    }

    @RepeatedTest(10)
    public void testIsPositiveReturnsTrue() {
        Random random = new Random();
        int numerator = random.nextInt(100) + 1;
        int denominator = random.nextInt(100) + 1;
//        System.out.printf("test with numerator: %d denominator : %d %n", numerator, denominator);
        Fraction fraction = new Fraction(numerator, denominator);
        Assertions.assertTrue(fraction.isPositive());
    }

    @RepeatedTest(10)
    public void testIsNegativeReturnsTrue() {
        Random random = new Random();
        int numerator = (random.nextInt(100) / -1) - 1;
        int denominator = (random.nextInt(100) / -1) - 1;
//        System.out.printf("test with numerator: %d denominator : %d %n", numerator, denominator);
        Fraction fraction = new Fraction(numerator, denominator);
        Assertions.assertTrue(fraction.isNegative());
    }

    @Test
    public void testIsPositiveReturnsFalseWhenNumIsNegativeAndDomPositive() {
        Fraction fraction = new Fraction(-8, 3);
        Assertions.assertFalse(fraction.isPositive());
    }

    @Test
    public void testIsPositiveReturnsFalseWhenNumIsPositiveAndDomNegative() {
        Fraction fraction = new Fraction(8, -3);
        Assertions.assertFalse(fraction.isPositive());
    }

    @Test
    public void testIsNegativeReturnsTrueWhenNumIsNegativeAndDomPositive() {
        Fraction fraction = new Fraction(-8, 3);
        Assertions.assertTrue(fraction.isNegative());
    }

    @Test
    public void testisNegativeReturnsTrueWhenNumIsPositiveAndDomNegative() {
        Fraction fraction = new Fraction(8, -3);
        Assertions.assertTrue(fraction.isNegative());
    }


    @Test
    public void testisNegativeReturnsFalseWhenNumIsPositiveAndDomPositive(){
        Fraction fraction = new Fraction(8, 3);
        Assertions.assertFalse(fraction.isNegative());
    }




}