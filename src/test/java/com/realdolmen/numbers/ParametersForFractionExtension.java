package com.realdolmen.numbers;

import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class ParametersForFractionExtension  {

    public static Stream<Arguments> summation() {
        return Stream.of(
                Arguments.of(2, 4, 2, 3, "7/6"),
                Arguments.of(1, 3, 8, 7, "31/21"),
                Arguments.of(1, 3, -1, 6, "1/6"),
                Arguments.of(-1, 3, -1, 6, "-1/2"),
                Arguments.of(1, 3, 0, 6, "1/3"),
                Arguments.of(-1, -3, -9, -6, "11/6")
        );
    }

    public static Stream<Arguments> subtraction() {
        return Stream.of(
                Arguments.of(2, 4, 2, 3, "-1/6"),
                Arguments.of(1, 3, 8, 7, "-17/21"),
                Arguments.of(1, 3, -1, 6, "1/2"),
                Arguments.of(-1, 3, -1, 6, "-1/6"),
                Arguments.of(1, 3, 0, 6, "1/3"),
                Arguments.of(-1, -3, -9, -6, "-7/6")
        );
    }

    public static Stream<Arguments> multiplication() {
        return Stream.of(
                Arguments.of(2, 4, 2, 3, "1/3"),
                Arguments.of(1, 3, 8, 7, "8/21"),
                Arguments.of(1, 3, -1, 6, "-1/18"),
                Arguments.of(-1, 3, -1, 6, "1/18"),
                Arguments.of(1, 3, 0, 6, "0"),
                Arguments.of(-1, -3, -9, -6, "1/2")
        );
    }

    public static Stream<Arguments> division() {
        return Stream.of(
                Arguments.of(2, 4, 2, 3, "3/4"),
                Arguments.of(1, 3, 8, 7, "7/24"),
                Arguments.of(1, 3, -1, 6, "-2"),
                Arguments.of(-1, 3, -1, 6, "2"),
                Arguments.of(-1, -3, -9, -6, "2/9")
        );
    }


}
