package com.realdolmen.numbers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.InputMismatchException;

public class CalculatorTest {


    private final Calculator calculator = new Calculator(); //SUT

    @Test
    public void testThatSumResultsInAPositiveNumber() {
        int result = calculator.sum(9, 1);
        Assertions.assertEquals(10, result, "Sum of 2 pos numbers results in a positive result");
    }

    @Test
    public void testThatSumOfPosAndNegResultsInANegative() {
        int result = calculator.sum(4, -6);
        Assertions.assertEquals(-2, result);
    }

    @Test
    public void testThatSumResultsInZero() {
        int result = calculator.sum(0, 0);
        Assertions.assertEquals(0, result);
    }

    @Test
    public void testThatSumAcceptsString() {
        int result = calculator.sum("8", "9");
        Assertions.assertEquals(17, result);
    }

    @Test
    public void testThatSumThrowsAnException() {
        Assertions.assertThrows(InputMismatchException.class, () -> calculator.sum("a", "9"));
    }

    @Test
    public void testThatSumAcceptsBigNumbers() {
        long result = calculator.sum(Integer.MAX_VALUE + 1, 0);
        Assertions.assertEquals(Integer.MAX_VALUE + 1, result);
    }

    @Test
    public void testThatSumAcceptsDouble() {
        double result = calculator.sum(0.5, 0.1);
        Assertions.assertEquals(0.6, result);
    }

    @Test
    public void testThatSumAcceptsOneDouble() {
        double result = calculator.sum(5.0, 1);
        Assertions.assertEquals(6.0, result);
    }

    @Test
    public void testMultiplication() {
        double result = calculator.multiplication(4, 2);
        Assertions.assertEquals(8, result);
    }

    @Test
    @Disabled("This test is disabled because it is not cool")
    public void testSomething() {
        int sum = calculator.sum(0, 0);
        Assertions.assertEquals(0, 0);
    }

    @Test
    public void testDemo() {
        int sum = calculator.returnRandomNumber();
        Assumptions.assumeTrue(sum == 5);
    }

    @ParameterizedTest
    @CsvSource({"2,false", "3,true", "4,false"})
    public void testIsOdd(String number, String result) {
        Assertions.assertEquals(Boolean.parseBoolean(result), Calculator.isOdd(Integer.parseInt(number)));
    }

}
