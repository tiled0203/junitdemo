package com.realdolmen.extensions;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class CustomExtension implements BeforeEachCallback, AfterEachCallback {
    @Override
    public void afterEach(ExtensionContext context) throws Exception {
//        System.out.println("end " +context.getTestMethod().get());
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
//        System.out.println("begin " + context.getTestMethod().get());
    }
}
