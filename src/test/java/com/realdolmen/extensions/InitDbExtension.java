package com.realdolmen.extensions;

import com.realdolmen.mockito.*;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.Date;


public class InitDbExtension implements BeforeEachCallback, AfterEachCallback {
    private final PersonRepository jdbcPersonRepository = new JdbcPersonRepository();

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        City city = new City("Some city", "1400");
        Address address = new Address("BigStreet", "4A", city);
        Person person = new Person("John", "Wicked", new Date(), address);
        jdbcPersonRepository.save(person);
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {

    }

}
