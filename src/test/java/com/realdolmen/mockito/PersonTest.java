//package com.realdolmen.mockito;
//
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.*;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import java.util.Date;
//
//import static org.mockito.ArgumentMatchers.anyInt;
//import static org.mockito.Mockito.*;
//
//@ExtendWith(MockitoExtension.class)
//public class PersonTest {
//    @Mock
//    private PersonRepository personRepository; //MOCK
//
//    @InjectMocks
//    private Person person; //SUT isolate me!
//
//    @Captor
//    ArgumentCaptor<Integer> argumentCaptor;
//
//    @BeforeEach
//    void setUp() {
//        Address address = new Address("street", "4A", new City("city", "4000"));
//        person = new Person("name", "lastname", new Date(), address);
//      MockitoAnnotations.openMocks(this);
////        person.setPersonRepository(personRepository); //inject the mock in the SUT
//
//        lenient().doNothing().when(personRepository).save(person); // seems unnecessary, but we stub with strict argument matching
//        lenient().when(personRepository.find(anyInt())).thenReturn(person);
//    }
//
//    @AfterEach
//     void afterEach() {
//        Mockito.verifyNoMoreInteractions(personRepository);
//    }
//
//    @Test
//    void testSave() {
//        person.save();
//        Mockito.verify(personRepository).save(person);
//    }
//
//    @Test
//    void testFindById() {
//        Person person = this.person.findBy(1);
//        Mockito.verify(personRepository).find(argumentCaptor.capture());
//        Assertions.assertEquals(1,argumentCaptor.getValue());
//        Assertions.assertEquals("4A", person.getAddress().getNumber());
//    }
//}
