package com.realdolmen.mockito;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class PersonV2Test {
    @Captor
    ArgumentCaptor<Integer> argumentCaptor;
    @Mock
    private PersonRepository personRepository; //MOCK
    @InjectMocks
    private PersonService personService; //SUT isolate me!

    @BeforeEach
    public void setUp() {
        Address address = new Address("street", "4A", new City("city", "4000"));
        Person responsePerson = new Person("name", "lastname", new Date(), address);
        responsePerson.setId(1);
        lenient().when(personRepository.find(anyInt())).thenReturn(responsePerson); //SIMULATE BEHAVIOR
    }

    @AfterEach
    public void afterEach() {
        Mockito.verifyNoMoreInteractions(personRepository);
    }

    @Test
    public void testSave() {
        Address address = new Address("blabla", "blabla", new City("blabla", "blabla"));
        Person person = new Person("blabla", "blabla", new Date(), address);
        personService.save(person);
        Mockito.verify(personRepository).save(person);
    }

    @Test
    public void testFindById() {
        Person person = this.personService.findBy(1);
        Mockito.verify(personRepository).find(argumentCaptor.capture());
        Assertions.assertEquals(1, argumentCaptor.getValue());
        Assertions.assertEquals("4A", person.getAddress().getNumber());
        Assertions.assertEquals(1, person.getId());
    }

    @Test
    public void testFindById8() {
        Person person = this.personService.findBy(8);
        Mockito.verify(personRepository).find(argumentCaptor.capture());
        Assertions.assertEquals(8, argumentCaptor.getValue());
        Assertions.assertEquals("4A", person.getAddress().getNumber());
    }
}
