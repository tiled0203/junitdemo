package com.realdolmen.mockito;

public class PersonService {
    private PersonRepository personRepository = new JdbcPersonRepository();

    public void save(Person person) {
        personRepository.save(person);
    }

    public Person findBy(int id) {
        Person person = personRepository.find(id);
//        person.setId(894);
        return person;
    }
}
