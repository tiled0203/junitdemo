package com.realdolmen.mockito;

import java.util.Date;

public class Person { // SUT

//    private transient PersonRepository personRepository = new JdbcPersonRepository();

    private Integer id;
    private final String firstName;
    private final String lastName;
    private final Date birthDate;
    private final Address address;

    public Person(String firstName, String lastName, Date birthDate, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.address = address;
    }

//    public void setPersonRepository(PersonRepository personRepository) {
//        this.personRepository = personRepository;
//    }

    void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Address getAddress() {
        return address;
    }

    public String name() {
        return firstName + " " + lastName;
    }

//    public void save(){
//        personRepository.save(this);
//    }
//
//    public Person findBy(int id) {
//        return personRepository.find(id);
//    }
}
