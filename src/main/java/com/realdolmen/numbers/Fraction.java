package com.realdolmen.numbers;

import com.realdolmen.numbers.utilities.NumberUtil;

public class Fraction {
    private double numerator;
    private double denominator;

    public Fraction(double numerator, double denominator) {
        if (denominator == 0.0) {
            throw new ArithmeticException("Cannot divide by 0");
        }
        this.numerator = numerator;
        this.denominator = denominator;
        simplify();
    }

    public Fraction(String numerator, String denominator) {
        try {
            this.denominator = Double.parseDouble(denominator);
            if (this.denominator == 0.0) {
                throw new ArithmeticException("Cannot divide by 0");
            }
            this.numerator = Double.parseDouble(numerator);
            simplify();
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Cannot convert String to a number");
        }
    }

    public int getNumerator() {
        return (int) numerator;
    }

    public int getDenominator() {
        return (int) denominator;
    }

    void simplify() {
        double commonFactor = NumberUtil.greatestCommonFactor(this.numerator, this.denominator);
        this.denominator = this.denominator / commonFactor;
        this.numerator = this.numerator / commonFactor;
    }

    public Fraction add(Fraction fraction) {
        int ad = this.getNumerator() * fraction.getDenominator();
        int bc = this.getDenominator() * fraction.getNumerator();
        int bd = this.getDenominator() * fraction.getDenominator();
        return new Fraction((ad + bc), bd);
    }

    public Fraction subtract(Fraction fraction) {
        int ad = this.getNumerator() * fraction.getDenominator();
        int bc = this.getDenominator() * fraction.getNumerator();
        int bd = this.getDenominator() * fraction.getDenominator();
        return new Fraction((ad - bc), bd);
    }

    public Fraction multiply(Fraction fraction) {
        int ad = this.getNumerator() * fraction.getNumerator();
        int bd = this.getDenominator() * fraction.getDenominator();
        return new Fraction(ad, bd);
    }

    public Fraction divide(Fraction fraction) {
        return multiply(new Fraction(fraction.getDenominator(), fraction.getNumerator())); //simply switch Numerator and Denominator and multiply it with the 1st fraction
    }

    @Override
    public String toString() {
        if(toDouble() % 1 == 0){
            return String.valueOf((int) toDouble());
        }
        return getNumerator() + "/" + getDenominator();
    }

    public double toDouble(){
        return this.getNumerator() / this.denominator;
    }

    public boolean isPositive(){
        return getDenominator() > 0 && getNumerator() > 0;
    }

    public boolean isNegative(){
        return getDenominator() < 0 | getNumerator() <= 0;
    }

}
