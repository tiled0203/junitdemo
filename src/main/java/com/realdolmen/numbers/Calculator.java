package com.realdolmen.numbers;

import java.util.InputMismatchException;
import java.util.Random;

public class Calculator {

    public int sum(int a, int b) {
        return a + b;
    }

    public int sum(String a, String b) {
        try {
            return Integer.parseInt(a) + Integer.parseInt(b);
        } catch (NumberFormatException e) {
            throw new InputMismatchException("input must be a number");
        }
    }

    public double sum(double a, double b) {
        return a + b;
    }

    public double multiplication(int a, int b) {
        return a * b;
    }

    public int returnRandomNumber() {
        return new Random().nextInt(10);
    }

    public static boolean isOdd(int number){
        return number % 2 != 0;
    }
}




